import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

public class EmitterImpl extends EmitterPOA {

	@Override
	public void sendMessage(String receiver, String message) {

		System.out.println(receiver + " a envoy� le message suivant : " + message);

		Receiver receiv = null;
		ReceiverImpl receiverImpl = new ReceiverImpl();

		try {
			receiv = ReceiverHelper.narrow(_default_POA().servant_to_reference(receiverImpl));
		} catch (ServantNotActive | WrongPolicy e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		receiv.receive(receiver, message);

	}

}
